/* notebook-application.c
 *
 * Copyright 2021 Christian Hergert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "notebook-application.h"
#include "notebook-window.h"

struct _NotebookApplication
{
  AdwApplication  parent_instance;
  GtkCssProvider *css;
};

G_DEFINE_TYPE (NotebookApplication, notebook_application, ADW_TYPE_APPLICATION)

NotebookApplication *
notebook_application_new (gchar *application_id,
                          GApplicationFlags  flags)
{
  return g_object_new (NOTEBOOK_TYPE_APPLICATION,
                       "application-id", application_id,
                       "flags", flags,
                       NULL);
}

static void
notebook_application_finalize (GObject *object)
{
  NotebookApplication *self = (NotebookApplication *)object;

  g_clear_object (&self->css);

  G_OBJECT_CLASS (notebook_application_parent_class)->finalize (object);
}

static void
notebook_application_activate (GApplication *app)
{
  GtkWindow *window;

  g_assert (GTK_IS_APPLICATION (app));

  window = gtk_application_get_active_window (GTK_APPLICATION (app));
  if (window == NULL)
    window = g_object_new (NOTEBOOK_TYPE_WINDOW,
                           "application", app,
                           "default-width", 800,
                           "default-height", 600,
                           NULL);

  gtk_window_present (window);
}

static void
notebook_application_startup (GApplication *app)
{
  NotebookApplication *self = (NotebookApplication *)app;

  g_assert (NOTEBOOK_IS_APPLICATION (self));

  G_APPLICATION_CLASS (notebook_application_parent_class)->startup (app);

  self->css = gtk_css_provider_new ();
  gtk_css_provider_load_from_resource (self->css, "/org/gnome/notebook/stylesheet.css");
  gtk_style_context_add_provider_for_display (gdk_display_get_default (),
                                              GTK_STYLE_PROVIDER (self->css),
                                              GTK_STYLE_PROVIDER_PRIORITY_THEME+1);
}

static void
notebook_application_class_init (NotebookApplicationClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GApplicationClass *app_class = G_APPLICATION_CLASS (klass);

  object_class->finalize = notebook_application_finalize;
  app_class->activate = notebook_application_activate;
  app_class->startup = notebook_application_startup;
}

static void
notebook_application_show_about (GSimpleAction *action,
                                 GVariant      *parameter,
                                 gpointer       user_data)
{
  NotebookApplication *self = NOTEBOOK_APPLICATION (user_data);
  GtkWindow *window = NULL;
  const gchar *authors[] = {"Christian Hergert", NULL};

  g_return_if_fail (NOTEBOOK_IS_APPLICATION (self));

  window = gtk_application_get_active_window (GTK_APPLICATION (self));

  gtk_show_about_dialog (window,
                         "program-name", "notebook",
                         "authors", authors,
                         "version", "0.1.0",
                         NULL);
}


static void
notebook_application_init (NotebookApplication *self)
{
  GSimpleAction *quit_action = g_simple_action_new ("quit", NULL);
  g_signal_connect_swapped (quit_action, "activate", G_CALLBACK (g_application_quit), self);
  g_action_map_add_action (G_ACTION_MAP (self), G_ACTION (quit_action));

  GSimpleAction *about_action = g_simple_action_new ("about", NULL);
  g_signal_connect (about_action, "activate", G_CALLBACK (notebook_application_show_about), self);
  g_action_map_add_action (G_ACTION_MAP (self), G_ACTION (about_action));

  const char *accels[] = {"<primary>q", NULL};
  gtk_application_set_accels_for_action (GTK_APPLICATION (self), "app.quit", accels);
}
