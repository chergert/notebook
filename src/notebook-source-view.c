/* notebook-source-view.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include "notebook-document.h"
#include "notebook-source-view.h"

struct _NotebookSourceView
{
  GtkSourceView parent_instance;
};

G_DEFINE_FINAL_TYPE (NotebookSourceView, notebook_source_view, GTK_SOURCE_TYPE_VIEW)

enum {
  PROP_0,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

GtkWidget *
notebook_source_view_new (void)
{
  return g_object_new (NOTEBOOK_TYPE_SOURCE_VIEW, NULL);
}

static void
notebook_source_view_finalize (GObject *object)
{
  NotebookSourceView *self = (NotebookSourceView *)object;

  G_OBJECT_CLASS (notebook_source_view_parent_class)->finalize (object);
}

static void
notebook_source_view_get_property (GObject    *object,
                                   guint       prop_id,
                                   GValue     *value,
                                   GParamSpec *pspec)
{
  NotebookSourceView *self = NOTEBOOK_SOURCE_VIEW (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
notebook_source_view_set_property (GObject      *object,
                                   guint         prop_id,
                                   const GValue *value,
                                   GParamSpec   *pspec)
{
  NotebookSourceView *self = NOTEBOOK_SOURCE_VIEW (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
notebook_source_view_class_init (NotebookSourceViewClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = notebook_source_view_finalize;
  object_class->get_property = notebook_source_view_get_property;
  object_class->set_property = notebook_source_view_set_property;

  g_type_ensure (NOTEBOOK_TYPE_DOCUMENT);
}

static void
notebook_source_view_init (NotebookSourceView *self)
{
  gtk_widget_add_css_class (GTK_WIDGET (self), "notebook");
}
