/* notebook-document.c
 *
 * Copyright 2021 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include "notebook-document.h"

struct _NotebookDocument
{
  GtkSourceBuffer  parent_instance;
  GtkTextTag      *p_spacing;
};

G_DEFINE_FINAL_TYPE (NotebookDocument, notebook_document, GTK_SOURCE_TYPE_BUFFER)

enum {
  PROP_0,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

NotebookDocument *
notebook_document_new (void)
{
  return g_object_new (NOTEBOOK_TYPE_DOCUMENT, NULL);
}

static void
notebook_document_constructed (GObject *object)
{
  NotebookDocument *self = (NotebookDocument *)object;
  GtkSourceLanguageManager *lm = gtk_source_language_manager_get_default ();
  GtkSourceStyleSchemeManager *sm = gtk_source_style_scheme_manager_get_default ();

  g_assert (NOTEBOOK_IS_DOCUMENT (self));

  G_OBJECT_CLASS (notebook_document_parent_class)->constructed (object);

  gtk_source_buffer_set_language (GTK_SOURCE_BUFFER (self),
                                  gtk_source_language_manager_get_language (lm, "markdown"));
  gtk_source_buffer_set_style_scheme (GTK_SOURCE_BUFFER (self),
                                      gtk_source_style_scheme_manager_get_scheme (sm, "Adwaita"));

  /* Even though this says "lines", it's really "paragraphs" */
  self->p_spacing = gtk_text_buffer_create_tag (GTK_TEXT_BUFFER (self), NULL,
                                                "pixels-above-lines", 4,
                                                "pixels-below-lines", 4,
                                                NULL);
}

static void
notebook_document_insert_text (GtkTextBuffer *buffer,
                               GtkTextIter   *location,
                               const char    *text,
                               int            length)
{
  NotebookDocument *self = (NotebookDocument *)buffer;
  GtkTextIter begin, end;

  g_assert (GTK_IS_TEXT_BUFFER (buffer));

  GTK_TEXT_BUFFER_CLASS (notebook_document_parent_class)->insert_text (buffer, location, text, length);

  gtk_text_buffer_get_bounds (GTK_TEXT_BUFFER (self), &begin, &end);
  gtk_text_buffer_apply_tag (GTK_TEXT_BUFFER (buffer), self->p_spacing, &begin, &end);
}

static void
notebook_document_finalize (GObject *object)
{
  NotebookDocument *self = (NotebookDocument *)object;

  G_OBJECT_CLASS (notebook_document_parent_class)->finalize (object);
}

static void
notebook_document_get_property (GObject    *object,
                                guint       prop_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
  NotebookDocument *self = NOTEBOOK_DOCUMENT (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
notebook_document_set_property (GObject      *object,
                                guint         prop_id,
                                const GValue *value,
                                GParamSpec   *pspec)
{
  NotebookDocument *self = NOTEBOOK_DOCUMENT (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
notebook_document_class_init (NotebookDocumentClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkTextBufferClass *text_buffer_class = GTK_TEXT_BUFFER_CLASS (klass);

  object_class->constructed = notebook_document_constructed;
  object_class->finalize = notebook_document_finalize;
  object_class->get_property = notebook_document_get_property;
  object_class->set_property = notebook_document_set_property;

  text_buffer_class->insert_text = notebook_document_insert_text;
}

static void
notebook_document_init (NotebookDocument *self)
{
}
